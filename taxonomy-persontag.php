<?php

use UCPeopleCompat\UCPeople\UCPeople;

get_header();

$ucPeople = new UCPeople();

?>
<main role="main" aria-label="Content" id="main-content" class="people-wrapper">
  <h1 class="archive-title persontag-archive-title"><?php the_archive_title(); ?></h1>
  <section id="archive" class="people-container">
    <div class="people-grid tax-content-container">
      <?php
        $displayConfig = [
          'infoToDisplay' => [
            'photo',
            'first_name',
            'last_name',
            'title',
            'email',
            'phone'
          ],
          'layout' => 'grid',
          'perRow' => '4'
        ];

        $ids = $ucPeople->getPeopleIDsByTaxonomy('persontag');

        if (count($ids)) {
          $displayConfig['peopleIDs'] = $ids;
          $ucPeople->displayPeople($displayConfig);
        }

      ?>
    </div>
  </section>
</main>

<?php

get_template_part('template-parts', 'pagination');

get_footer();

?>