<?php

namespace Lobo\Lib;

class ScriptLoader {

  private $buildDir;

  public function __construct()
  {
    $this->buildDir = wp_get_environment_type() === 'local' ? '/build' : '/dist';
  }

  public function get_typography_choice() {
    return get_theme_mod('typography_choice', 'sans');
  }

  public function get_color_scheme_choice() {
    return get_theme_mod('scheme_choice', 'uconn');
  }

  public function prepare_google_fonts() {

    $fonts = 'Roboto:wght@100;300;400;600;700&display=swap';

    if ($this->get_typography_choice() === 'admissions') {
      $fonts = 'Archivo:wght@400;600;700&family=DM+Serif+Display&family=' . $fonts;
    }

    $query_args = [ 'family' => $fonts ];

    wp_register_style(
      'google-fonts',
      add_query_arg($query_args, 'https://fonts.googleapis.com/css2'),
      [],
      null
    );
  }

  public function prepare_adobe_fonts() {
    wp_register_style(
      'adobe-fonts',
      'https://use.typekit.net/nmk2dgu.css',
      [],
      null
    );
  }

  public function enqueue_css() {

    $is_builder_active =  Helpers::is_builder_active();
    $is_builder_enabled = Helpers::is_builder_enabled();
    $customizer_color_sheet = LOBO_THEME_URL . $this->buildDir . '/customizer-' . $this->get_color_scheme_choice() . '.css';
    $customizer_typography_sheet = LOBO_THEME_URL . $this->buildDir . '/customizer-' . $this->get_typography_choice() . '.css'; 


    wp_enqueue_style('font-awesome', 'https://use.fontawesome.com/releases/v5.7.2/css/all.css', false);

    $this->prepare_google_fonts();
    wp_enqueue_style('google-fonts');

    // admissions typography requires a call to adobe fonts. no poiint doing it if it's not needed.
    if ($this->get_typography_choice() === 'admissions') {
      $this->prepare_adobe_fonts();
      wp_enqueue_style('adobe-fonts');
    }

    wp_enqueue_style('banner', LOBO_THEME_URL . '/assets/css/banner.css');

    wp_enqueue_style('a11y-menu', LOBO_THEME_URL . '/vendor/ucomm/a11y-menu/dist/main.css');

    if (!$is_builder_enabled) {
      wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css');
    }


    wp_enqueue_style('lobo-style', LOBO_THEME_URL . $this->buildDir . '/main.css', array('banner', 'a11y-menu'));

    // enqueue customizer styles
    wp_enqueue_style('lobo-customizer-colors', $customizer_color_sheet, ['lobo-style']);
    wp_enqueue_style('lobo-customizer-typography', $customizer_typography_sheet, [ 'lobo-style' ]);
  }
  public function enqueue_js() {

    $is_builder_active = Helpers::is_builder_active();

    wp_register_script('a11y-menu', LOBO_THEME_URL . '/vendor/ucomm/a11y-menu/dist/Navigation.min.js', array(), false, true);

    wp_enqueue_script('a11y-menu');

    wp_enqueue_script('index-script', LOBO_THEME_URL . $this->buildDir . '/main.js', array('jquery', 'a11y-menu'), false, true);

    wp_localize_script('index-script', 'ajaxObj', array(
      'isBuilderActive' => $is_builder_active
    ));
  }
}