<?php

namespace Lobo\Lib;

class Footer {
  public static function display_primary_menu() {
    $footer_layout = get_theme_mod('footer_layout');

    if ($footer_layout !== 'centered') {
      $theme_location = 'primary_footer';
      $args = array(
        'theme_location' => $theme_location,
        'menu_id' => 'primary-footer-menu',
        'container' => 'nav',
        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
      );
      if (has_nav_menu($theme_location)) {
        wp_nav_menu($args);
      }
    }

  }
  public static function display_bottom_menu() {
    $theme_location = 'footer';
    $args = array(
      'theme_location' => $theme_location,
      'menu_id' => 'lower-footer-menu',
      'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    );
    if (has_nav_menu($theme_location)) {
      wp_nav_menu($args);
    }
  }
  static function create_nav_menu() {
    $is_builder_active = Helpers::is_builder_active();

    if (!$is_builder_active) {
      get_template_part('includes/mobile-nav');
    }
  }

  static public function display_wordmark($wordmark_src) {
    include_once LOBO_THEME_DIR . '/includes/footer-wordmark.php';
  }
}