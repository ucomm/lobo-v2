<?php

namespace Lobo\Lib;

class Menus {
  static public function register_menus() {
    $theme_menus = array(
      'footer' => 'Footer Menu',
      'header' => 'Main Menu',
      'hero_menu' => 'Hero Menu',
      'primary_footer' => 'Primary Footer Menu'
    );

    if (class_exists('Castor_Public')) {
      $theme_menus['castor_sidebar'] = 'Castor Sidebar Menu';
    }

    foreach ($theme_menus as $slug => $name) {
      register_nav_menu($slug, __($name), 'lobo');
    }
  }
  static public function modify_nav_menu_attributes($atts, $item, $args) {
    if (in_array('menu-item-has-children', $item->classes)) {
      $atts['aria-haspopup'] = 'true';
      $atts['aria-label'] = 'submenu';
    }
    return $atts;
  }
}