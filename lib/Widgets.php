<?php

namespace Lobo\Lib;

class Widgets {
  public static function init() {
    self::register_widgets();
  }

  private static function register_widgets() {
    $widgets = array(
      array(
        'name' => __('Footer Widget 1', 'lobo'),
        'id' => 'footer_widget_1',
        'before_widget' => '<div class="widget footer-widget">',
        'after_widget' => '</div>'
      ),
      array(
        'name' => __('Footer Widget 2', 'lobo'),
        'id' => 'footer_widget_2',
        'before_widget' => '<div class="widget footer-widget">',
        'after_widget' => '</div>'
      ),
      array(
        'name' => __('Footer Widget 3', 'lobo'),
        'id' => 'footer_widget_3',
        'before_widget' => '<div class="widget footer-widget">',
        'after_widget' => '</div>'
      ),
      array(
        'name' => __('Footer Widget 4', 'lobo'),
        'id' => 'footer_widget_4',
        'before_widget' => '<div class="widget footer-widget">',
        'after_widget' => '</div>'
      )
    );

    foreach ($widgets as $i => $w) {
      register_sidebar($w);
    }

  }
}