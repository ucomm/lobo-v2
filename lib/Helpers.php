<?php

namespace Lobo\Lib;

use stdClass;

class Helpers {
   /**
   * Ensure that the last item in the menu will be the copyright info.
   *
   * @param string $menu - the slug of the menu to check
   * @return bool/string - returns false if the menu has copyright info. otherwise add the info to the list of menu items.
   */
  static public function copyright_handler() {
    $text = "university of connecticut";

    $theme_locations = get_nav_menu_locations();
    $footer_location = get_term($theme_locations['footer'], 'nav_menu');
    $slug = $footer_location->slug;
    $menu_items = wp_get_nav_menu_items($slug);
    $last_item = $menu_items[count($menu_items) - 1];

    if (stripos($last_item->post_title, $text) === false) {
      // create a new menu item by filtering the menu slug.
      $filter_name = 'wp_nav_menu_' . $slug . '_items';

      add_filter($filter_name, function($items) {
        // create copyright text and list item.
        $cr_text = '© ' . date('Y') . ' University of Connecticut';
        $cr_item = '<li class="menu-item menu-item-type-custom"><a>' . $cr_text . '</a></li>';

        // add to the $items string
        $items .= $cr_item;

        return $items;
      });
    } 
    return false;
  }

  /**
   * Ensure that if copyright text exists in the menu that the date stays current.
   *
   * @param string $title
   * @param object $item
   * @param array $args
   * @param int $depth
   * @return string
   */
  static public function filter_nav_item_title($title, $item, $args, $depth) {
    $text = "university of connecticut";

    // only filter if the item matches.
    if ($args->theme_location !== 'footer' || stripos($title, $text) === false) {
      return $title;
    }

    $cr_symbol  = "©";
    $current_year = date('Y');

    $cr_position = stripos($title, $cr_symbol);
    $text_position = stripos($title, $text);
    
    if ($text_position !== 0 && preg_match('/[©A-Za-z0-9 ]/', $title)) {
      
      $parts = explode(' ', $title);

      // check for the © symbol and the correct year.
      if ($parts[0] !== $cr_symbol && $parts[0] !== $current_year) {

        //  change the year
        $parts[0] = $current_year;

        // put the symbol at the beginning of the array
        array_unshift($parts, $cr_symbol);

      } elseif ($parts[0] === $cr_symbol && $parts[1] !== $current_year) {
        $parts[1] = $current_year;
      }

      $title = implode(' ', $parts);

    } elseif ($text_position === 0) {
      $title = $cr_symbol . " " . $current_year . " " . $title;
    }

    return $title;
  }
  public static function generate_site_title() {
    $title = '';
    if (!is_front_page()) {
      $title .= "University of Connecticut " . get_bloginfo('name');
      $title .= " | " . get_the_title(); 
    } else {
      $title .= "University of Connecticut"; 
      $title .= " | " . get_bloginfo('name');
    }
    return $title;
  }
  /**
   * A wrapper for checking if the beaver builder page builder is active.
   *
   * @return boolean
   */
  public static function is_builder_active() {
    if (class_exists('FLBuilderModel')) {
      return \FLBuilderModel::is_builder_active();
    } else {
      return false;
    }
  }
  /**
   * A wrapper for checking if beaver builder is currently being used on a page.
   *
   * @return boolean
   */
  public static function is_builder_enabled() {
    if (class_exists('FLBuilderModel')) {
      return \FLBuilderModel::is_builder_enabled();
    } else {
      return false;
    }
  }

  /**
   * Modifies the builder config array to ensure that admins and editors have the ability to use unfiltered HTML
   *
   * @param array $config
   * @return array|boolean
   */
  public function filterUIConfigCallback($config) {
    $user = wp_get_current_user();
    $withCap = $user->has_cap('edit_others_posts');
    return $withCap ? true : $config;
  }

  public static function updateBodyClass(array $classes = []) {
    $typography_choice = get_theme_mod('typography_choice') . '-lobo-typography';
    array_push($classes, $typography_choice);
    return $classes;
  }

  public static function set_read_more_text(string $more): string {
    global $post;
    return ' <a class="moretag" href="' . get_permalink($post->ID) . '" aria-hidden="true">Read More...</a>';
  }

  public static function add_post_supports() {
    add_post_type_support('page', ['excerpt', 'revisions']);
  }
}