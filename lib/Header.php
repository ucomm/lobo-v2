<?php

namespace Lobo\Lib;

/**
 * 
 * Handles fetching and displaying parts for the header.
 * 
 */
class Header {
  public static function init() {
    self::display_title_row();
    self::display_main_nav();
    self::display_cta_menu();
  }

  private static function display_cta_menu() {
    include_once(LOBO_THEME_DIR . '/includes/hero-cta-menu.php');
  }
  private static function display_main_nav() {
    include_once(LOBO_THEME_DIR . '/includes/main-nav-row.php');
  }
  private static function display_title_row() {
    include_once(LOBO_THEME_DIR . '/includes/title-row.php');
  }
  public static function do_alert_banner() {
    if (class_exists('Alert_Banner')) {
      if (method_exists('Alert_Banner', 'show_alerts_in_content')) {
        \Alert_Banner::show_alerts_in_content();
      }
    }
  }
}