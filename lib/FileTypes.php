<?php

namespace Lobo\Lib;

class FileTypes {
  /**
   * Adds Proximan Nova and other custom fonts to the theme customizer
   * and page builder font list
   */
  static public function add_custom_fonts_to_list() {
    $custom_fonts = array(
      'Proxima Nova' => array(
        'fallback' => 'Verdana, Arial, Helvetica, sans-serif',
        'weights' => array(
          'default',
          '300',
          '400',
          '600',
          '700'
        )
      )
    );
    
    if (get_theme_mod('typography_choice') === 'admissions') {
      $admissions_fonts = [
        'Archivo' => [
          'fallback' => '"Proxima Nova", Verdana, Arial, Helvetica, sans-serif',
          'weights' => [
            'default',
            '400',
            '600',
            '700'
          ]
        ],
        'DM Serif Display' => [
          'fallback' => 'times, serif',
          'weights' => [
            '400',
          ]
        ],
        'league-gothic' => [
          'fallback' => '"Proxima Nova", Verdana, Arial, Helvetica, sans-serif',
          'weights' => [
            '400'
          ]
        ]
      ];

      $custom_fonts = array_merge($custom_fonts, $admissions_fonts);
    }

    foreach ($custom_fonts as $name => $settings) {
      // Add to customizer
      if (class_exists('FLFontFamilies') && isset(\FLFontFamilies::$system)) {
        \FLFontFamilies::$system[$name] = $settings;
      }

      // Add to page builder
      if (class_exists('FLBuilderFontFamilies') && isset(\FlBuilderFontFamilies::$system)) {
        \FLBuilderFontFamilies::$system[$name] = $settings;
      }
    }
  }

  /**
   * Add support for featured images and custom image sizes
   *
   * @return void
   */
  static public function add_image_sizes() {
    add_theme_support('post-thumbnails');
    self::create_image_sizes();
  }

  /**
   * Creates a custom shortcode for using svgs directly from the server.
   * Legacy code and may be dropped if a different solution is reached.
   *
   * @param array $atts
   * @return string - html string
   */
  static public function svg_handler($atts) {
    // This is needed because WP turns svgs into images that (mostly) can't be styled with css.
    // Turn an svg into a link.
    $directory = LOBO_THEME_URL . '/images/svgs/';
    $a = shortcode_atts(array(
      'name' => '',
      'prefix' => 'fab',
      'class' => 'social-svg',
      'directory' => 'brands',
      'url' => ''
    ), $atts);
    $file = $directory . $a['name'] . '.svg';
    $url = $a['url'];

    $html = "<a class='social-svg-link' href='" . $url . "' target='_blank' rel='noreferrer'>";
    $html .= "<span class='screen-reader'>Follow us on " . $a['name'] . "</span>";
    if (!is_plugin_active('font-awesome/index.php')) {
      $html .= file_get_contents(OBJECT_STORAGE_FONTAWESOME_URL . '/svgs' . '/' . $a['directory'] . '/' . $a['name'] . '.svg');
    } else {
      $html .= do_shortcode('[icon prefix="' . $a['prefix'] . '" name="' . $a['name'] . '" class="' . $a['class'] . ']');
    }
    $html .= "</a>";
    return $html;
  }
  /**
   * Allow non-standard filetypes to be uploaded to the WP media library.
   *
   * @param array $existing_mimes
   * @return array
   */
  static public function upload_mime_types($existing_mimes = array()) {
    $existing_mimes['zip'] = 'application/zip';
    $existing_mimes['svg'] = 'image/svg+xml';
    return $existing_mimes;
  }
  /**
   * Create custom image sizes from an array.
   *
   * @return void
   */
  private static function create_image_sizes() {
    $image_sizes = self::get_image_sizes();

    foreach ($image_sizes as $name => $size) {
      add_image_size($name, $size['width'], $size['height']);
    }
  }
  /**
   * Returns an array of custom image sizes for social media
   *
   * @return array
   */
  private static function get_image_sizes() {
    return array(
      'fb-twitter-share' => array(
        'readable_name' => __('FB/Twitter Share 1.9:1', 'uconn-2019'),
        'width' => 1200,
        'height' => 630
      ),
      'instagram-share' => array(
        'readable_name' => __('Instagram Share 1:1', 'uconn-2019'),
        'width' => 1080,
        'height' => 1080
      ),
      'linkedin-share' => array(
        'readable_name' => __('LinkedIn Share 1.4:1', 'uconn-2019'),
        'width' => 1200,
        'height' => 857
      ),
      'snapchat-share' => array(
        'readable_name' => __('Snapchat Share 9:16', 'uconn-2019'),
        'width' => 1080,
        'height' => 1920
      )
    );
  }
}