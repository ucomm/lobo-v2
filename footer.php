<?php
$footer_layout = get_theme_mod('footer_layout');
$wordmark_src = Lobo\Lib\Customizer::get_footer_wordmark_src();
?>
  <footer class="<?php echo $footer_layout; ?>">
    <section class="lobo-row">
      <div class="lobo-container nav-container">
      <?php
        if ($wordmark_src !== '') {
          Lobo\Lib\Footer::display_wordmark($wordmark_src);
        }
        Lobo\Lib\Footer::display_primary_menu(); 
      ?>
      </div>
    </section>
    
    <section class="lobo-row">
      <div class="lobo-container widget-container">
        <?php
          for ($i = 1; $i < 5; $i++) {
            $widget = 'footer_widget_' . $i;
            if (is_active_sidebar($widget)) {
              dynamic_sidebar($widget);
            }
          }
        ?>
      </div>
    </section>

    <section class="lobo-row">
      <div class="lobo-container bottom-container">
          <?php Lobo\Lib\Footer::display_bottom_menu(); ?>
      </div>
    </section>
  </footer>
  <?php

    Lobo\Lib\Footer::create_nav_menu();

    wp_footer();

  ?>
</div>
</body>
</html>
