/*
This is the main JS file for the theme.
 */

import '../sass/main.scss'
import 'mmenu-js/dist/mmenu.js'

import doMMenu from './mmenu-config'
import observer from './observer'

document.addEventListener('DOMContentLoaded', () => {
  // enable a11y-menu JS
  const navigation = new Navigation({ click: true });
  navigation.init();

  // configure the mobile menu
  const mobileNav = document.querySelector('#mobile-nav')

  // only configure if BB is not activated
  if (mobileNav !== null) {
    doMMenu()
    observer
  }
})