const doMMenu = () => {
  const menuIcon = document.querySelector('#hamburger-menu-icon')

  const mobileMenu = new Mmenu('#mobile-nav', {
    extensions: [
      'position-right',
    ]
  })

  const mmenuAPI = mobileMenu.API
  const menuList = document.querySelector('nav#mobile-nav .mm-panel > ul[aria-label="mobile navigation menu list"]')
  const mmBtns = menuList.querySelectorAll('a[aria-label="submenu"]')
  const mobileCTAs = document.querySelector('#menu-hero-menu')
  const mobileSearchContainer = document.querySelector('#mobile-search-container > form')

  if (mobileCTAs == null) { 
    menuList.after(mobileSearchContainer)
  } else { 
    mobileCTAs.className = ''
    mobileCTAs.classList.add('mm-listview')
    mobileCTAs.querySelector('.mm-navbar').style = 'display: none'
    menuList.after(mobileCTAs, mobileSearchContainer)
  }


  // change the href of parent items to point at the next mmenu panel view
  mmBtns.forEach((btn) => {
    const nextView = btn.nextElementSibling.getAttribute('href')
    btn.setAttribute('href', nextView)
  })

  menuIcon.addEventListener('click', () => {
    mmenuAPI.open()
  })
}

export default doMMenu