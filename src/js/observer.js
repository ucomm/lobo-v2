/**
 * 
 * Detect classname changes on the `body` and update the hamburger menu icon
 * 
 * @param array mutationsList an array of DOM elements that have changed durning an event
 */

const callback = (mutationsList) => {
  mutationsList.forEach(({ attributeName }) => {
    if (attributeName === 'class') {
      const menuOpened = document.body.classList.contains('mm-wrapper--opened')
      const hamburger = document.querySelector('#hamburger-menu-icon')
      menuOpened ? hamburger.classList.add('is-active') : hamburger.classList.remove('is-active')
    }
  });
}
const observer = new MutationObserver(callback)
observer.observe(document.body, { attributes: true })

export default observer