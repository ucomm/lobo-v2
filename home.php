<?php get_header(); ?>

<main class="lobo-blog-index">
  <!-- keep an h1 for accessibility -->
  <h1 style="position: absolute; left: -9999px;">
    <?php bloginfo('name'); ?>
  </h1>
  <section>
    <?php if (have_posts()) {
      while (have_posts()) {
        the_post();
    ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php
          // only display the title for interior pages
          if (!Lobo\Lib\Helpers::is_builder_enabled() && !is_front_page()) {
          ?>
            <div class="title-container">
              <a href="<?php the_permalink(); ?>">
                <h2 class="post-title">
                  <?php the_title(); ?>
                </h2>
              </a>
            </div>
          <?php
          }
          ?>
          <div class="content-container">
            <?php the_excerpt(); ?>
          </div>
        </article>
    <?php
      }
    } ?>
    <div class="lobo-pagination-container">
      <?php 
        the_posts_pagination([
          'screen_reader_text' => ' '
        ]); 
      ?>
    </div>
  </section>
</main>

<?php get_footer(); ?>