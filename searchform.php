<?php
  $site_title = get_bloginfo('name');
  $search_text = "Search $site_title";
?>

<form action="<?php echo home_url() ?>" method="get" role="search" class="uc-search-form" title="<?php echo esc_attr_x('Type and press Enter to search.', 'Search form mouse hover title.', 'lobo'); ?>">
  <input aria-label="Search <?php echo $site_title; ?>" role="search" type="text" class="uc-search-input" name="s" class="form-control" value="<?php echo esc_attr_x( $search_text, 'Search form field placeholder text.', 'lobo' ); ?>" onfocus="if (this.value == '<?php echo esc_attr_x( $search_text, 'Search form field placeholder text.', 'lobo' ); ?>') { this.value = ''; }" onblur="if (this.value == '') this.value='<?php echo esc_attr_x( $search_text, 'Search form field placeholder text.', 'lobo' ); ?>';">
  <button type="submit" class="uc-search-btn" value="<?php _e( 'Submit', 'lobo'); ?>"><span class="screen-reader">Press enter to search</span><i class="fa fa-search" aria-hidden="true"></i></button>

</form>
