<?php

/**
 * 
 * Template Name: People
 * Notes: The fle _must_ be named user-people.php for ACF meta boxes to appear
 * 
 */

add_filter('body_class', function($classes) {
  $classes[] = 'page-people';
  return $classes;
});

get_header();

$groups = get_terms([
  'taxonomy' => 'group',
  'orderby' => 'slug',
  'hide_empty' => false
]);

$personTag = get_terms([
  'taxonomy' => 'persontag',
  'orderby' => 'slug',
  'hide_empty' => false
]);

?>

<main id="main-content">
  <section class="people-wrapper">
    <?php 
      if (have_posts()) {
        while (have_posts()) {
          the_post();
    ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php the_content(); ?>
      </article>
      <div class="people-container">
      <?php
        }
      }
      ?>
      <?php 
        get_template_part('template-parts/content', 'people'); 
      // end people container 
      ?>
      </div>
  </section>
</main>

<?php 

get_footer();