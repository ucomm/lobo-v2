<?php

define('LOBO_THEME_DIR', get_stylesheet_directory());
define('LOBO_THEME_URL', get_stylesheet_directory_uri());

if (!defined('OBJECT_STORAGE_URL')) {
  define('OBJECT_STORAGE_URL', 'https://ucommobjectstorage.blob.core.windows.net/uconn-cdn-files');
}

if (!defined('OBJECT_STORAGE_FONTAWESOME_URL')) {
  define('OBJECT_STORAGE_FONTAWESOME_URL', OBJECT_STORAGE_URL . '/shared/icons/legacy-font-awesome-6.4.2');
}

require_once(LOBO_THEME_DIR . '/vendor/autoload.php');

require_once( 'lib/Customizer.php' );
require_once( 'lib/FileTypes.php' );
require_once( 'lib/Footer.php' );
require_once( 'lib/Header.php' );
require_once( 'lib/Helpers.php' );
require_once( 'lib/Menus.php' );
require_once( 'lib/ScriptLoader.php' );
require_once( 'lib/Widgets.php' );

// select the right autoload.php file depending on environment.

use Lobo\Lib\ScriptLoader;
use Lobo\Lib\Helpers as Helpers; 

// add custom fonts to beaver builder defaults
if (class_exists('FLBuilder')) {
  add_action('init', array('Lobo\Lib\FileTypes', 'add_custom_fonts_to_list'));
  $helpers = new Helpers(); 
  add_filter('fl_user_has_unfiltered_html', [ $helpers, 'filterUIConfigCallback' ]);
}

// register menus and widgets
add_action('after_setup_theme', array('Lobo\Lib\Menus', 'register_menus'));
add_action('widgets_init', array('Lobo\Lib\Widgets', 'init'));

// ensure accurate copyright info in the footer
// if (!is_admin()) {
// 	Lobo\Lib\Helpers::copyright_handler();
// }

add_filter('nav_menu_item_title', array('Lobo\Lib\Helpers', 'filter_nav_item_title'), 4, 10);

// register customizer areas
add_action('customize_register', array('Lobo\Lib\Customizer', 'register_footer_customization'), 11);

// filter menus for accessibility
add_filter('nav_menu_link_attributes', array('Lobo\Lib\Menus','modify_nav_menu_attributes'), 10, 3);

// enqueue styles and scripts

$scriptLoader = new ScriptLoader();

add_action('wp_enqueue_scripts', [$scriptLoader, 'enqueue_js']);

add_action('wp_enqueue_scripts', [$scriptLoader, 'enqueue_css'], 9);

// allow svgs and zips to be uploaded to the media library
add_filter('upload_mimes', array('Lobo\Lib\FileTypes', 'upload_mime_types'));

// add support for featured images and custom image sizes
add_action('after_setup_theme', array('Lobo\Lib\FileTypes', 'add_image_sizes'));

// shortcodes
add_shortcode('svg_handler', array('Lobo\Lib\FileTypes', 'svg_handler'));

// customizer css
add_action('wp_head', array('Lobo\Lib\Customizer', 'print_css'));


// filter body class to include typography choice
add_filter('body_class', ['Lobo\Lib\Helpers', 'updateBodyClass']);

add_filter('excerpt_more', ['Lobo\Lib\Helpers', 'set_read_more_text']);

add_action('init', ['Lobo\Lib\Helpers', 'add_post_supports']);