<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<title><?php echo Lobo\Lib\Helpers::generate_site_title(); ?></title>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta property="og:url" content="<?php echo get_permalink(); ?>" />
	<meta property="og:title" content="<?php echo Lobo\Lib\Helpers::generate_site_title(); ?>" />
	<meta property="og:image" content="<?php
																			if (!has_post_thumbnail()) {
																				echo LOBO_THEME_URL . '/images/uconn-placeholder-3-2.jpg';
																			} else {
																				the_post_thumbnail_url();
																			}
																			?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="icon" href="<?php echo get_stylesheet_directory_uri() . "/images/favicon.ico;" ?>" type="image/x-icon" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope="itemscope" itemtype="http://schema.org/WebPage">
	<?php Lobo\Lib\Header::do_alert_banner(); ?>
	<div id="page-wrapper">
		<script type='text/javascript' src='https://ucommobjectstorage.blob.core.windows.net/cookie-jar/cookie-notification.js'></script>
		<noscript>
			<p>Our websites may use cookies to personalize and enhance your experience. By continuing without changing your cookie settings, you agree to this collection. For more information, please see our <a href="https://privacy.uconn.edu/university-website-notice/" target="_blank">University Websites Privacy Notice</a>.</p>
		</noscript>
		<a href="#main-content" class="screen-reader-link">Skip Navigation</a>
		<header>
			<div class="banner-row">
				<?php
					get_template_part('template-parts/partial', 'banner');	
				?>
			</div>
			<?php
			Lobo\Lib\Header::init();
			?>
		</header>