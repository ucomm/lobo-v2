<?php

use UCPeopleCompat\UCPeople\UCPeople;

$ucPeople = new UCPeople();
$is_builder_enabled = Lobo\Lib\Helpers::is_builder_enabled();
$fields = get_fields();
$groupInfo = $ucPeople->getPersonTermInfo(get_the_ID(), 'group');
$personTagInfo = $ucPeople->getPersonTermInfo(get_the_ID(), 'persontag');
$sidebarFields = [
  [
    'value' => $fields['email'],
    'label' => 'Email',
    'hasLink' => true,
    'link' => 'mailto:' . $fields['email']
  ],
  [
    'value' => $fields['phone'],
    'label' => 'Phone',
    'hasLink' => false,
    'link' => null
  ],
  [
    'value' => $fields['office_location'],
    'label' => 'Office Location',
    'hasLink' => false,
    'link' => null
  ],
  [
    'value' => $fields['campus'],
    'label' => 'Campus',
    'hasLink' => false,
    'link' => null
  ],
  [
    'value' => $fields['url'],
    'label' => 'Link',
    'hasLink' => true,
    'link' => $fields['url']
  ],
  [
    'value' => $groupInfo,
    'label' => 'Groups',
    'hasLink' => null,
    'link' => null
  ],
  [
    'value' => $personTagInfo,
    'label' => 'Tags',
    'hasLink' => null,
    'link' => null
  ]
];


get_header();
?>

<main aria-label="Content" id="main-content">
  <section class="person-content max-w-screen-lg px-4">
    <?php
    if (have_posts()) {
      while (have_posts()) {
        the_post();
    ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <section class="info-section">
            <div class="headshot-container">
              <div class="image-container">
                <?php 
                  if (has_post_thumbnail()) {
                    the_post_thumbnail();
                  } else {
                    echo "<img src='" . UC_PEOPLE_COMPAT_URL . '/assets/img/placeholder-uconn.jpg' . "' class='attachment-post-thumbnail size-post-thumbnail wp-post-image' loading='lazy' />";
                  }
                ?>
              </div>
            </div>
            <div class="title-container">
              <h1 class="person-name"><?php the_title(); ?></h1>
              <p class="person-title">
                <?php echo $ucPeople->breakField($fields['title']); ?>
              </p>
              <p class="person-department">
                <?php echo $ucPeople->breakField($fields['department']); ?>
              </p>
            </div>
          </section>
          <section class="bio-section">
            <div class="content-container">
              <?php echo $fields['about']; ?>
            </div>
            <div class="sidebar-container">
              <?php
              foreach ($sidebarFields as $field) {
                get_template_part('template-parts/partial', 'single-person-sidebar', $field);
              }
              ?>
            </div>
          </section>
        </article>
    <?php
      }
    } else {
      get_template_part('template-parts/content', 'none');
    }
    ?>
  </section>
</main>

<?php get_footer(); ?>