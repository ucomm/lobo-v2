<?php 
  get_header();
  $is_builder_enabled = Lobo\Lib\Helpers::is_builder_enabled();
  $does_paginate_by_category = get_theme_mod('does_paginate_by_category', false);
?>

<main>
  <section>
    <?php
      $is_builder_enabled ?
      get_template_part('template-parts/content', 'loop-bb') :
      get_template_part('template-parts/content', 'loop'); 
      ?>
  </section>
  <section>
    <div class="lobo-container lobo-pagination-container">
      <div>
        <?php
          previous_post_link('&laquo; %link', '%title', $does_paginate_by_category);
        ?>
      </div>
      <div>
        <?php
          next_post_link('%link &raquo;', '%title', $does_paginate_by_category);
        ?>
      </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>