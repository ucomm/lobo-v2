<?php get_header(); ?>

<main>
	<section>
		<div class="lobo-row">
			<div class="lobo-container search-title-container">
				<h1 class="search-title">Search results for: <?php echo get_search_query(); ?></h1>
			</div>
		</div>
		<?php
			if (have_posts()) {
				while (have_posts()) {
					the_post();
					include(LOBO_THEME_DIR . '/includes/search-content.php');
				}
			} else {
		?>
			<div class="lobo-container search-results-container">
				<p>No results found.</p>
			</div>
		<?php
			}
		?>
	</section>
</main>

<?php get_footer(); ?>