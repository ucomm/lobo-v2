<?php get_header(); ?>

<main>
  <section>
    <?php get_template_part('template-parts/content', 'loop'); ?>
  </section>
</main>

<?php get_footer(); ?>