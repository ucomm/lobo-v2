<?php 
  get_header(); 
  $is_builder_enabled = Lobo\Lib\Helpers::is_builder_enabled();
?>

<main>
  <section>
    <?php
      if (!$is_builder_enabled) {
        get_template_part('template-parts/content', 'loop');
      } else {
        get_template_part('template-parts/content', 'loop-bb');
      }
    ?>
  </section>
</main>

<?php get_footer(); ?>