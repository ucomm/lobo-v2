#!/bin/bash

# check if the ucomm-plugins directory exists
ssh deploy@"${TARGET_SERVER}" "test -d ${UCOMM_THEMES_DIR} || mkdir ${UCOMM_THEMES_DIR}"

# rsync to avoid issues where multiple projects are pushing at the same time
rsync -avzO -e ssh --chmod=ugo=rwX --no-perms \
  "${WORKSPACE}/theme-dist/out.zip" deploy@${TARGET_SERVER}:"${UCOMM_THEMES_DIR}/${PROJECT_SLUG}.zip"