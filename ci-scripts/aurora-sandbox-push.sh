#!/bin/bash

user="${AURORA_DEV_USER}"
pass="${AURORA_DEV_PASSWORD}"
host="${AURORA_DEV_HOST}"
remotepath="${AURORA_DEV_THEMES_DIR}/${PROJECT_SLUG}"

lftp sftp://$user:$pass@$host:/$remotepath -e "mirror -Rv --exclude .entrypoint --exclude ci-scripts --exclude banner.sh --exclude .browserslistrc --exclude .nvmrc --exclude bitbucket-pipelines.yml --exclude composer.json --exclude composer.lock --exclude custom.ini --exclude docker-compose.yml --exclude excludes.txt --exclude Jenkinsfile --exclude package-lock.json --exclude package.json --exclude postcss.config.js  --exclude README.md --exclude tailwind.config.js --exclude webpack.config.js --exclude .git --exclude node_modules --exclude www --delete ./ ;bye" -p 22 22