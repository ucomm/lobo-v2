#!/bin/bash

# require manual override of --dry-run on tagged pushes to prod
DRY_RUN=--dry-run

# while the jenkins param is boolean, it will evaluate as a string in this script
if [ "$RSYNC_DRY_RUN" = "false" ]
  then DRY_RUN=""
fi

# the path in the current working directory to the project
source="./"
sitepath="$COMM_PRODUCTION_SITES_DIR/$SITE_DIRECTORY/public_html"
projectpath="$sitepath/content/themes/$PROJECT_SLUG"
path_exists=$(ssh comms test -d $projectpath || echo "notexists")

if [ ! -n "$PROJECT_SLUG" ] || [ "$path_exists" = "notexists" ]; then
  echo "The theme slug is not set or the project path does not exist on comm0."
  exit 1
fi

# push to comm0 using --dry-run as needed
rsync $DRY_RUN -avzO -e ssh --chmod=ugo=rwX --no-perms \
--exclude-from "excludes.txt" \
--delete $source comms:$projectpath