#!/bin/bash

# the path in the current working directory to the project
source="./"

# paths to project on staging0
sitepath="$COMM_STAGING_SITES_DIR/$SITE_DIRECTORY/public_html"
projectpath="$sitepath/content/themes/$PROJECT_SLUG"
path_exists=$(ssh staging0 test -d $projectpath || echo "notexists")

if [ ! -n "$PROJECT_SLUG" ] || [ "$path_exists" = "notexists" ]; then
  echo "The theme slug is not set or the project path does not exist on staging0."
  exit 1
fi

# push to staging0
rsync -avzO -e ssh --chmod=ugo=rwX --no-perms \
--exclude-from "excludes.txt" \
--delete $source staging0:$projectpath