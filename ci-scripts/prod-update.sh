#!/bin/bash

cd "${JENKINS_HOME}/.wp-cli"

# if we're doing a dry run, prompt for a manual review of the zip file contents and exit
if [ "$WP_CLI_DRY_RUN" != "false" ]; then
  echo "DRY RUN: ${THEME_SLUG} pushed to ${UCOMM_THEMES_DIR}"
  echo "DRY RUN: manually review the contents of the zip file"
  # report on the current version of the theme in the target environment
  wp "${TARGET_ALIAS}" theme get "${PROJECT_SLUG}" --fields=version,name,status --skip-plugins --skip-themes
  exit 0
fi

wp "${TARGET_ALIAS}" theme install "${UCOMM_THEMES_DIR}/${PROJECT_SLUG}.zip" --force --skip-plugins --skip-themes

# report on the updated version of the theme
wp "${TARGET_ALIAS}" theme get "${PROJECT_SLUG}" --fields=version,name,status --skip-plugins --skip-themes