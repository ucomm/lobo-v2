#!/bin/bash

# read in a list of directories and files to include in the zip
zip -r "${WORKSPACE}/theme-dist/out.zip" -@ < "${WORKSPACE}/zip-includes.txt"

# the packeton directory will be deleted at the end of the build
# so we list the included files as a record just in case there's a discrepancy
unzip -l "${WORKSPACE}/theme-dist/out.zip"