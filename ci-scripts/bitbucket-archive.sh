#!/bin/bash

if [ ! -f "${WORKSPACE}/theme-dist/out.zip" ]; then
  echo "No out.zip file found in theme-dist directory"
  exit 1
fi

# bitbucket credentials are in jenkins and exposed as global variables
curl --location --request POST \
  --user "${BITBUCKET_API_USERNAME}:${BITBUCKET_API_PASSWORD}" \
  "https://api.bitbucket.org/2.0/repositories/ucomm/${PROJECT_SLUG}/downloads" \
  --form files=@"${WORKSPACE}/theme-dist/out.zip;filename=${PACKAGE_VERSION}.zip"