#!/bin/bash

cd "${JENKINS_HOME}/.wp-cli"

wp "${TARGET_ALIAS}" theme install "${UCOMM_THEMES_DIR}/${PROJECT_SLUG}.zip" --force --skip-plugins --skip-themes

wp "${TARGET_ALIAS}" theme get "${PROJECT_SLUG}" --fields=version,name,status --skip-plugins --skip-themes