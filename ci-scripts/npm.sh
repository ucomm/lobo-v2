#!/bin/bash

# source nvm to use the proper node version
# additional node versions should be installed using the dedicated jenkins job
source $HOME/.nvm/nvm.sh

# use the correct version
nvm use
# install dependencies for CI environments
npm ci
# run the build script
npm run build