pipeline {
  agent any
  environment {
    PROJECT_SLUG = "lobo-v2"
    DEV_BRANCH = "develop"
    PROD_BRANCH = "master"
    FEATURE_BRANCH = "feature/*"
  }
  // handle rsync dry runs
  parameters {
    booleanParam(
      name: 'RSYNC_DRY_RUN',
      defaultValue: true,
      description: 'Toggles the rsync --dry-run flag'
    )
  }
  stages {
    // checkout from git and skip if the "ci skip" message is present in the commit
    stage('Checkout') {
      steps {
        // do not delete builds. there's a breaking issue there
        // https://issues.jenkins.io/browse/JENKINS-66843
        scmSkip(deleteBuild: false)
        // relies on the global notification library
        // https://bitbucket.org/ucomm/jenkins-send-notifications/src/main/
        sendNotifications 'STARTED'
      }
    }
    stage("Prepare Build Assets") {
      // run these tasks at the same time
      parallel {
        stage('NPM') {
          steps {
            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
              sh "${WORKSPACE}/ci-scripts/npm.sh"
            }
          }
        }
        stage('Composer') {
          steps {
            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
              sh "${WORKSPACE}/ci-scripts/composer.sh"
            }
          }
        }
      }
      post {
        always {
          echo "======== $BRANCH_NAME end asset builds ========"
        }
        success {
          echo "======== success - $BRANCH_NAME asset builds ========"
        }
        failure {
          sendNotifications("FAILURE - $BRANCH_NAME asset builds")
        }
      }
    }
    stage('Dev Pushes') {
      // set environment variables 
      environment {
        FILENAME = "$GIT_BRANCH"
        SITE_DIRECTORY = "edu.uconn.devredesign"
      }
      when {
        anyOf {
          branch "${DEV_BRANCH}";
          branch "${FEATURE_BRANCH}"
        }
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/dev-push.sh"
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to dev")
        }
      }
    }
    stage('Aurora Sandbox Push') {
      when {
        anyOf {
          branch "${FEATURE_BRANCH}";
          branch "${DEV_BRANCH}"
        }
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/aurora-sandbox-push.sh"
        }
      }
      post {
        failure {
          sendNotifications("AURORA SANDBOX PUSH FAILED")
        }
      }
    }
    stage('Dev - bitbucket archive') {
      when {
        branch "${DEV_BRANCH}";
      }
      environment {
        FILENAME = "$GIT_BRANCH"
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
        }
      }
    }
    stage('Staging Push') {
      environment {
        SITE_DIRECTORY = "edu.uconn.staging"
      }
      when {
        branch "${PROD_BRANCH}"
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/dev-push.sh"
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to staging")
        }
      }
    }
    stage('Prod Push') {
      environment {
        SITE_DIRECTORY = "edu.uconn"
        FILENAME = "$TAG_NAME"
      }
      when {
        buildingTag()
      }
      parallel { 
        stage('Push tag to prod') {
          steps {
            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
              sh "${WORKSPACE}/ci-scripts/prod-push.sh"
            }
          }
        }
        stage('Archive tag to bitbucket') {
          steps {
            script {
              if (env.RSYNC_DRY_RUN == "false") {
                catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                  sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
                }
              }
            }
          }
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to prod")
        }
        success {
          // NB - don't use script blocks very much.
          // I'm just adding this so th comm0-updates channel doesn't get flooded on dry runs
          script {
            if (env.RSYNC_DRY_RUN == "false") {
              sendNotifications("SUCCESS", "#comm0-updates", [
                [
                  type: "section",
                  text: [
                    type: "mrkdwn",
                    text: ":tada: *$PROJECT_SLUG* updated"
                  ]              
                ]
              ])
              slackUploadFile(channel: "#comm0-updates", filePath: "changelog.md", initialComment:  "${PROJECT_SLUG} Changelog")
            }
          }
        }
      }
    }
  }
  post {
    success {
      sendNotifications 'SUCCESSFUL'
    }
    failure {
      sendNotifications 'FAILED'
    }
    aborted {
      sendNotifications 'ABORTED'
    }
    always {
      echo "======== Cleanup ========"
      sh "rm -rf node_modules"
    }
  }
}