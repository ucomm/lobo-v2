pipeline {
  agent any
  environment {
    PROJECT_SLUG = "lobo-v2"
    DEV_BRANCH = "develop"
    PROD_BRANCH = "master"
    FEATURE_BRANCH = "feature/*"
  }
  options {
    buildDiscarder(logRotator(numToKeepStr: '5'))
    disableConcurrentBuilds()
  }
  parameters {
    booleanParam(
      name: 'WP_CLI_DRY_RUN',
      defaultValue: true,
      description: 'Perform a dry run of the installation for production'
    )
  }
  stages {
    // checkout from git and skip if the "ci skip" message is present in the commit
    stage('Checkout') {
      steps {
        // do not delete builds. there's a breaking issue there
        // https://issues.jenkins.io/browse/JENKINS-66843
        scmSkip(deleteBuild: false)
        // relies on the global notification library
        // https://bitbucket.org/ucomm/jenkins-send-notifications/src/main/
        sendNotifications 'STARTED'
      }
    }
    stage('Determine Version') {
      steps {
        script {
          if (env.TAG_NAME) {
            env.PACKAGE_VERSION = env.TAG_NAME
          } else {
            env.PACKAGE_VERSION = "dev-${env.GIT_BRANCH.replaceAll("/", "-")}"
          }
          echo "PACKAGE_VERSION: ${env.PACKAGE_VERSION}"
        }
      }
    }
    stage("Prepare Build Assets") {
      // run these tasks at the same time
      parallel {
        stage('NPM') {
          steps {
            catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
              sh "${WORKSPACE}/ci-scripts/npm.sh"
            }
          }
        }
        stage('Composer') {
          steps {
            catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
              sh "${WORKSPACE}/ci-scripts/composer.sh"
            }
          }
        }
      }
      post {
        always {
          echo "======== $BRANCH_NAME end asset builds ========"
        }
        success {
          echo "======== success - $BRANCH_NAME asset builds ========"
        }
        failure {
          sendNotifications("FAILURE - $BRANCH_NAME asset builds")
        }
      }
    }
    stage('Create Archive') {
      when {
        anyOf {
          branch "${FEATURE_BRANCH}"
          branch "${DEV_BRANCH}"
          branch "${PROD_BRANCH}";
          buildingTag()
        }
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/archive.sh"
        }
      }
      post {
        always {
          echo "======== $BRANCH_NAME end archive ========"
        }
        success {
          echo "======== success - $BRANCH_NAME archive ========"
        }
        failure {
          echo "======== failed - $BRANCH_NAME archive ========"
        }
      }
    }
    stage('Dev Pushes') {
      // set environment variables 
      environment {
        TARGET_SERVER = "${COMM_STAGING_SERVER}"
        TARGET_ALIAS = "@dev-devredesign"
      }
      when {
        anyOf {
          branch "${DEV_BRANCH}";
          branch "${FEATURE_BRANCH}"
        }
      }
      steps {
        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/project-push.sh"
        }
        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/dev-update.sh"
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to dev")
        }
      }
    }
    stage('Aurora Sandbox Push') {
      when {
        anyOf {
          branch "${FEATURE_BRANCH}";
          branch "${DEV_BRANCH}"
        }
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/aurora-sandbox-push.sh"
        }
      }
      post {
        failure {
          sendNotifications("AURORA SANDBOX PUSH FAILED")
        }
      }
    }
    stage('Staging Push') {
      environment {
        TARGET_SERVER = "${COMM_STAGING_SERVER}"
        TARGET_ALIAS = "@staging-uconn"
      }
      when {
        branch "${PROD_BRANCH}"
      }
      steps {
        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/project-push.sh"
        }
        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/dev-update.sh"
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to staging")
        }
      }
    }
    stage('Prod Push') {
      environment {
        TARGET_SERVER = "comm0-prd-web.its.uconn.edu"
        TARGET_ALIAS = "@prod-uconn"
      }
      when {
        buildingTag()
      }
      steps {
        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/project-push.sh"
        }
        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/prod-update.sh"
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to prod")
        }
        success {
          // NB - don't use script blocks very much.
          // I'm just adding this so th comm0-updates channel doesn't get flooded on dry runs
          script {
            if (env.RSYNC_DRY_RUN == "false") {
              sendNotifications("SUCCESS", "#comm0-updates", [
                [
                  type: "section",
                  text: [
                    type: "mrkdwn",
                    text: ":tada: *${PROJECT_SLUG}* updated"
                  ]              
                ]
              ])
              slackUploadFile(channel: "#comm0-updates", filePath: "changelog.md", initialComment:  "${PROJECT_SLUG} Changelog")
            }
          }
        }
      }
    }
    stage('Archive tag to bitbucket') {
      when {
        anyOf {
          branch "${DEV_BRANCH}"
          branch "${FEATURE_BRANCH}"
          branch "${PROD_BRANCH}"
          buildingTag()
        }
      }
      steps {
        //  catch if archiving tag fails
        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
        }
      }
    }
  }
  post {
    success {
      sendNotifications 'SUCCESSFUL'
    }
    failure {
      sendNotifications 'FAILED'
    }
    aborted {
      sendNotifications 'ABORTED'
    }
    always {
      echo "======== Cleanup ========"
      cleanWs(
        cleanWhenNotBuilt: true,
        deleteDirs: true,
        disableDeferredWipeout: false,
        notFailBuild: true,
        patterns: [
          [
            pattern: "node_modules",
            type: "INCLUDE"
          ],
          [
            pattern: "theme-dist",
            type: "INCLUDE"
          ]
        ]
      )
    }
  }
}