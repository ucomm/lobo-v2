# Changelog
## 2.5.6
- Fixed issue where people couldn't change images. Caused by deprecated Beaver Builder filter
- Updated jenkins deployment to favor wp cli over rsync
## 2.5.5
- Updated object storage urls from stackpath (rip) to azure
## 2.5.4
- Deprecated direct call to UConn banner project in favor of generated files through script
## 2.5.3
- Updates to jenkins deploy scripts
## 2.5.2
- Additional button style class
## 2.5.1
- Better support for fontawesome icons
## 2.5.0
- Added support for people and people taxonomy pages via the `uc-people` and `uc-people-compat` plugins
- Added support for Jenkinsfile
- Updated composer dependencies
- Updated docker compose
## 2.4.8
- fixed a broken font path
## 2.4.7
- Changed font family base url
## 2.4.6
- Changed cookie notice url
## 2.4.5
- added styles for links; hartford customizer style bug fix 
## 2.4.4
- updated mobile menu config so selectors do not rely on the name of the mobile menu in order to function 
## 2.4.3
- Fix for sidebar navigation links getting null href values
## 2.4.2
- fix for :visited button links
## 2.4.1
- updated bitbucket pipelines so that js/css assets build to correct directory
- updated mobile menu config so that mobile menu still functions even if hero menu does not exist
## 2.4.0
- updates to Customizer/Tailwind for custom regional campus styles
## 2.3.5
- Resolved bug where non-SuperAdmin users on multi-sites were unable to enter tags such as iframe
## 2.3.4
- Fixed missing excerpt box on pages
## 2.3.3
- Update to heading typography
- Local dev updates
## 2.3.2
- Updated ucomm/a11y-menu dependency.
## 2.3.1
- Fixed a bug where Beaver Builder typography settings weren't correct with the Admissions typography styles.
## 2.3.0
- Add pagination to posts
- Added customizer option to paginate posts by category
- Added a page template for a blog index with pagination
## 2.2.3
- Update to docker-compose file for local development
- Fixed typography issue of widget titles in footer for non-beaver builder pages
- Adjusted non-beaver builder pages to have h2 headers with hidden h1's for style.
## 2.2.2
- Update to customizer typography
## 2.2.1
- Fixed mobile nav styles
- Removed id from search form to avoid conflicts
- Updated dependencies
## 2.2.0
- Added admissions typography option to customizer
- Added better support for _not_ having a main menu
- Updated composer deps
- Keeping built assets in git
- Updated UConn banner style compatibility
## 2.1.8
- Update to alert banner needed to update theme
## 2.1.7
- Updated UConn banner to v3
## 2.1.6
- Updated nav menu with a version that works
## 2.1.5
- Adjusted table style to use sans-serif fonts
## 2.1.4
- Updated nav menu deps
## 2.1.3
- Adjusted external link styles to use FontAwesome 5
- Fixed nav css to prevent long names from wrapping
## 2.1.2
- Style adjustments to centered footer
- Added more social icon svgs
- Adjusted social icon svg color for new brand site color palette
- Added option for no wordmark in footer
## 2.1.1
- Added support for submenu border colors
## 2.1.0
- Fixed castor sidebar menu
- Added better support for beaver builder module/row spacing
## 2.0.5
- Added support for bootstrap tables
## 2.0.4
- Added `img-no-border` utility css class
## 2.0.3
- Updated menu for better IE support
- Fixed p tag margins
- Styled gravity form submit button
## 2.0.2
- Fixed styles to improve spacing and typography
## 2.0.1
- Updated screenshot and theme name for admin area to make it easier to distinguish from v1
- Updated bb-plugin dev dependency
## 2.0.0
- Many minor updates...
- Removed dependency on bb-theme
- Updated nav/nav styles
- Updated gulp
- Improved markup and accessibility
## 1.3.1
- Updated uconn/banner package
- Fixed style bugs for new navigation
- Fixed customizer bugs for new navigation
## 1.3.0
- Updated main nav menu and styles to use ucomm/a11y-menu
## 1.2.1
- Updated skip link anchor id
- Changed Hero image style
## 1.2.0
- Dependency updates for banner conflicts
## 1.1.19
- Removed absolute positioning from parent pages to handle pages with no hero images.
## 1.1.18
- Updated banner and added support for new font CDN
## 1.1.17
- Added support for footer images via the customizer
- Fixed customizer style bug
## 1.1.16
- Added favicon
## 1.1.15
- Fixed typography for headers and site name
## 1.1.14
- Fixed a bug for BB pipelines where assets weren't archived correctly.
## 1.1.13
- Added Privacy/GDPR notice scripts
## 1.1.12
- Fixed issue where if no "hero" menu was assigned the mobile nav menu would display incorrectly.
## 1.1.11
- Added hero CTAs to mobile menu
- Accessibility improvements
- Style updates
## 1.1.9
- Skip to content link on all pages
- Body links are underlined for accessibility
- Editors can publish pages so that the UConn Alert banner plugin works correctly
## 1.1.8
- Reset user capabilities to handle alert banner plugin 
## 1.1.7
- Fixed a bug where the footer wordmark selector wasn't showing in the customizer.
- Added wordmark for Waterbury campus
## 1.1.7
- Added header typography options for the customizer
## 1.1.6
- Well that almost worked... This version breaks out enqueuing js and css assets into two separate functions.
## 1.1.5
- Tried fixing bug related to bb-plugin
## 1.1.4
- Brought vendor folder back.
## 1.1.2
- Added customizer options for nav text colors.
- Added external-heading(-small, -medium, -large) and external-link(-small, -medium, -large) classes to display FontAwesome icons after outbound links as needed.
## 1.1.1
- Added new customizer app to make more granular changes to theme colors
- Changed site title style to handle issue related to BB Theme updates
- Tested with BB Theme v1.6.1 and BB Plugin 1.10.6.3