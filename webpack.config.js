const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin")

const prodEnv = process.env.NODE_ENV === 'production' ? true : false

let mode = 'development'
let buildDir = path.resolve(__dirname, 'build')

const optimization = {}

if (prodEnv) {
  mode = 'production'
  buildDir = path.resolve(__dirname, 'dist')
  optimization.minimize = true
  optimization.minimizer = [
    new CssMinimizerPlugin(),
    '...'
  ]
}

module.exports = {
  entry: {
    main: path.resolve(__dirname, 'src/js/index.js'),
    'customizer-admissions': path.resolve(__dirname, 'src', 'sass', 'customizer', 'customizer-admissions.scss'),
    'customizer-avery-point': path.resolve(__dirname, 'src', 'sass', 'customizer', 'customizer-avery-point.scss'),
    'customizer-brand': path.resolve(__dirname, 'src', 'sass', 'customizer', 'customizer-brand.scss'),
    'customizer-communications': path.resolve(__dirname, 'src', 'sass', 'customizer', 'customizer-communications.scss'),
    'customizer-hartford': path.resolve(__dirname, 'src', 'sass', 'customizer', 'customizer-hartford.scss'),
    'customizer-sans': path.resolve(__dirname, 'src', 'sass', 'customizer', 'customizer-sans.scss'),
    'customizer-serif': path.resolve(__dirname, 'src', 'sass', 'customizer', 'customizer-serif.scss'),
    'customizer-stamford': path.resolve(__dirname, 'src', 'sass', 'customizer', 'customizer-stamford.scss'),
    'customizer-uconn': path.resolve(__dirname, 'src', 'sass', 'customizer', 'customizer-uconn.scss'),
    'customizer-waterbury': path.resolve(__dirname, 'src', 'sass', 'customizer', 'customizer-waterbury.scss'),
  },
  output: {
    path: buildDir,
    filename: '[name].js',
    chunkFilename: '[name][ext]'
  },
  mode,
  optimization,
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    debug: true,
                    useBuiltIns: 'usage',
                    corejs: 3.13
                  }
                ],
              ]
            }
          }
        ]
      },
      {
        test: /\.s?css$/i,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              importLoaders: 2
            }
          },
          {
            loader: 'postcss-loader'
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              sassOptions: {
                includePaths: [
                  'node_modules/scss/**/*.scss'
                ]
              }
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.css', '.scss']
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
  ],
}