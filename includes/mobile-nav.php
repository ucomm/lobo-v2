<nav id="mobile-nav" role='navigation' aria-hidden="<?php echo has_nav_menu('header') ? 'false' : 'true'; ?>">
  <?php
    $menu_args = array(
      'theme_location' => 'header',
      'items_wrap' => '<ul id="mobile-%1$s" class="%2$s" aria-label="mobile navigation menu list">%3$s</ul>',
      'container' => false
    );
    $cta_args = array(
      'theme_location' => 'hero_menu',
      'menu-id' => 'mobile-cta-list',
      'container' => false
    );
    $menu = wp_nav_menu($menu_args);
    if (has_nav_menu($cta_args['theme_location'])) {
      $cta_menu = wp_nav_menu($cta_args);
    }
    ?>
  <div id="mobile-search-container">
    <?php echo get_search_form(); ?>
  </div>
</nav>

