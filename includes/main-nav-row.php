<div aria-hidden="<?php echo has_nav_menu('header') ? 'false' : 'true'; ?>">
  <?php
    			
    wp_nav_menu(array(
      'theme_location' => 'header',
      'items_wrap' => '<ul id="%1$s" class="am-click-menu %2$s">%3$s</ul>',
      'menu_id' => 'am-main-menu',
      'container' => 'nav',
      'container_id' => 'am-navigation',
      'container_class' => 'lobo-container',
      'walker' => new A11y\Menu_Walker()
    ));

  ?>
</div>