<div class="lobo-row mt-10 mb-5 md-lg:mb-0">
  <div class="lobo-container title-container justify-between">
    <div>
      <a class="title-link" href="<?php echo home_url(); ?>">
        <span class="title-text"><?php echo get_bloginfo('name'); ?></span>
      </a>
    </div>
    <button 
      id="hamburger-menu-icon" 
      class="hamburger hamburger--collapse" type="button" 
      aria-controls="mobile-nav"
      style="<?php echo has_nav_menu('header') ? '' : 'display: none;'; ?>"
    >
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
      <span class="screen-reader">Menu</span>
    </button>
    <div class="search-container hidden md-lg:block">
      <?php get_search_form(); ?>
    </div>
  </div>
</div>