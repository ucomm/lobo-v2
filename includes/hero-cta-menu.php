<?php

$menu_location = 'hero_menu';

if (has_nav_menu($menu_location) && is_front_page()) {
  $args = array(
    'theme_location' => $menu_location,
    'container' => 'nav',
    'container_id' => 'hero-menu-container',
    'menu_id' => 'hero-menu',
    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
  );
  wp_nav_menu($args);
}