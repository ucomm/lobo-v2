<div class="footer-logo-container">
  <a href="<?php echo home_url(); ?>">
    <img src="<?php echo $wordmark_src; ?>" alt="UConn wordmark">
  </a>
</div>