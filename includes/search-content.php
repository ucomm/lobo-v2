<div class="lobo-row">
  <div class="lobo-container search-results-container">
    <div class="search-link-container">
      <p>
        <strong>
          <a class="search-link" href="<?php echo get_the_permalink(); ?>">
            <?php the_title(); ?>
          </a>
        </strong>
      </p>
    </div>
    <div class="search-results-detail">
      <div class="thumbnail-container">
        <?php 
          if (has_post_thumbnail()) {
            the_post_thumbnail('medium');
          } else {
        ?>
            <img width="300" height="200" 
              src="<?php echo LOBO_THEME_URL . '/images/uconn-placeholder-3-2.jpg'; ?>" 
              aria-hidden="true" />
        <?php
          }
        ?>
      </div>
      <div class="excerpt-container">
        <?php the_excerpt(); ?>
      </div>
    </div>
  </div>
</div> 