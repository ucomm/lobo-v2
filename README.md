# Lobo Theme
Lobo is a child of Beaver Builder Theme. It requires the [UConn Banner composer package](https://github.com/uconn/banner) built by Brian Daley.

## Usage
### Getting started
```bash
$ composer install # only the first time
$ docker-compose up
```
### Accessing containers
To access a particular docker container, find the container name and then enter an interactive terminal.
```bash
$ docker ps # to get the container name
$ docker exec -it container_name /bin/bash
```
### Debugging Wordpress
Wordpress debug logs can be found inside the web container at `/etc/httpd/logs/error_log`

## Bitbucket
### Creating releases
Assuming you're using git flow, tag the release with the command `git flow release start {version_number}`. Tags must follow the [semver system](http://semver.org/). Follow these steps to complete a release
```bash
git tag # check the current tags/versions on the project
git flow release start {new_tag}
git flow release finish {new_tag}
# in the first screen add new info or just save the commit message as is
# in the second screen type in the same tag you just used and save.
git push --tags && git push origin master
git checkout develop
``` 
Finally re-run the pipeline build on the [satis repo](https://bitbucket.org/ucomm/composer-repository).
### Pipelines
This repo has a bitbucket pipelines (written by Adam Berkowitz) attached in case you wish to create zip file downloads for tags/branches. **You may exclude files/folders from the zip archive by adding them to composer.json. The syntax is the same as gitignore.** To enable pipelines on bitbucket, simply visit the project repo and enable pipelines at `repo -> settings -> Pipelines/settings`.
## Dependency management

It would be a good idea to keep UComm/WordPress dependencies in require-dev, and only keep functional dependencies in "require".  That way, your package will export with only the required files for your plugin/theme to function, and not include a full WP install. The only current non-dev requirement is the uconn/banner package.

## Known Issues
- When using Lobo as a dev dependency of a plugin project, you *must* remap the vendor directory in the plugin's `docker-compose.yml` like this:
  
```yml
volumes:
  - ./vendor:/var/www/html/content/themes/lobo/vendor
```
