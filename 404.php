<?php get_header(); ?>
<main>
  <section>
    <div class="lobo-row">
      <div class="lobo-container error-container">
        <h1>Sorry! That page doesn't seem to exist.</h1>
        <p>You can search for a different page.</p>
        <?php get_search_form(); ?>
      </div>
    </div>

  </section>

</main>
<?php get_footer(); ?>