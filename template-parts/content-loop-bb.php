<?php
  if (have_posts()) {
    while (have_posts()) {
      the_post();
?>  
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php the_content(); ?>
      </article>
<?php
    }
  } else {
    get_template_part('template-parts/content', 'none');
  }
?>