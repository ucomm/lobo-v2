<?php
if (!$args['value']) {
  return;
}
?>

<div class="person-info-item">
  <div class="item-label-container">
    <p class="item-label">
      <?php
      $label = $args['label'];
      if (false !== stripos($args['label'], 'groups')) {
        $label = 'Role';
      } else if (false !== stripos($args['label'], 'tags')) {
        $label = 'Department';
      }
      echo $label;
      ?>
    </p>
  </div>
  <div class="item-container">
    <?php
    if (is_array($args['value'])) {
      foreach ($args['value'] as $item) {
    ?>
        <p class="item-value">
          <?php
          if (!$item['hasLink']) {
            echo $item['value'];
          } else {
          ?>
            <a class="item-link" href="<?php echo $item['link']; ?>"><?php echo $item['value']; ?></a>
          <?php
          }
          ?>
        </p>
      <?php
      }
    } else {
      ?>
      <p class="item-value">
        <?php
        if (!$args['hasLink']) {
          echo $args['value'];
        } else {
        ?>
          <a class="item-link" href="<?php echo $args['link']; ?>"><?php echo $args['value']; ?></a>
        <?php
        }
        ?>
      </p>
    <?php
    }
    ?>
  </div>
</div>