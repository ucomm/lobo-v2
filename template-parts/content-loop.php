<div class="lobo-row">
  <div class="lobo-container">
  <?php
  if (have_posts()) {
    while (have_posts()) {
      the_post();
      ?>  
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <?php
        // only display the title for interior pages
        if (!Lobo\Lib\Helpers::is_builder_enabled() && !is_front_page()) {
      ?>
        <div class="title-container">
          <!-- keep an h1 for accessibility -->
          <h1 style="position: absolute; left: -9999px;">
            <?php bloginfo('name'); ?>
          </h1>
          <h2 class="post-title">
            <?php the_title(); ?>
          </h2>
        </div>
      <?php
        }
      ?>
        <div class="content-container">
          <?php
            if (has_post_thumbnail()) {
              the_post_thumbnail();
            } 
            the_content(); 
          ?>
        </div>
      </article>
    <?php
    }
  } else {
    get_template_part('template-parts/content', 'none');
  }
  ?>
  </div>
</div>